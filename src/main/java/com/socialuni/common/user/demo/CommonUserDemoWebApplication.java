package com.socialuni.common.user.demo;

import com.socialuni.uniapp.sdk.config.EnableUniAppSDK;
import com.socialuni.common.user.sdk.config.EnableCommonUserSDK;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableCommonUserSDK
@EnableUniAppSDK
public class CommonUserDemoWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonUserDemoWebApplication.class, args);
    }

}
