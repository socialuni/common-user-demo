package com.socialuni.common.user.demo.config;

import com.socialuni.common.user.demo.UserSdkException;
import com.socialuni.common.user.sdk.utils.CommonTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户未登录拦截器
 */
@Component
@Slf4j
public class UserAuthInterceptor implements HandlerInterceptor {
    /*
     * 进入controller层之前拦截请求
     * 在请求处理之前进行调用（Controller方法调用之前
     */
    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object o) throws UserSdkException {
        String token = CommonTokenUtil.getToken();
        String uri = req.getRequestURI();
        log.info(req.getMethod());
        log.info(uri);
        if ((req.getMethod().equals(RequestMethod.OPTIONS.name())
                || uri.equals("/")
                || uri.contains("login")
                || token != null)
        ) {
            return true;
        } else {
            throw new UserSdkException("请进行登录");
        }
    }
}