package com.socialuni.common.user.demo;

import lombok.Data;

/**
 * @author qinkaiyuan
 * @date 2019-02-14 22:10
 */
@Data
public class ResultRO<T> {
    //0成功，1，系统异常，2业务错误，3自定义错误
    private int errorCode = 0;
    private Boolean success = true;
    private String errorMsg = "请求成功";
    private T data;

    public ResultRO() {
    }

    public ResultRO(T data) {
        this.data = data;
    }

    public ResultRO(Integer errorCode) {
        this(errorCode, "系统错误，请重试或联系客服");
    }

    public ResultRO(Integer errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.success = false;
    }

    public ResultRO(Integer errorCode, String errorMsg, T data) {
        this(errorCode, errorMsg);
        this.data = data;
    }

    public ResultRO(ResultRO<?> resultRO) {
        this(resultRO.getErrorCode(), resultRO.getErrorMsg());
    }

    public Boolean hasError() {
        return !this.success;
    }
}
