package com.socialuni.common.user.demo.service;

import com.socialuni.common.user.model.DO.CommonTokenDO;
import com.socialuni.common.user.model.DO.CommonUserDO;
import com.socialuni.common.user.sdk.domain.CommonUserDODomainCreate;
import com.socialuni.common.user.sdk.manage.CommonUserTokenCreate;
import com.socialuni.common.user.sdk.model.RO.CommonUserRO;
import com.socialuni.common.user.sdk.utils.model.CommonUserROUtil;
import com.socialuni.uniapp.sdk.exception.UniSdkException;
import com.socialuni.uniapp.sdk.model.UniProviderLoginQO;
import com.socialuni.common.user.sdk.model.RO.UniLoginRO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;

@Service
public class LoginService {
    @Resource
    CommonUserTokenCreate commonUserTokenCreate;
    @Resource
    CommonUserDODomainCreate commonUserDODomainCreate;

    //通过渠道信息返回用户
    @Transactional
    public UniLoginRO<CommonUserRO> providerLogin(UniProviderLoginQO loginQO) throws UniSdkException {
        //创建或返回
        CommonUserDO user = commonUserDODomainCreate.getOrCreate(loginQO);
        CommonUserRO userRO = CommonUserROUtil.getMineUser(user);
        CommonTokenDO socialUserTokenDO = commonUserTokenCreate.create(user.getId());

        return new UniLoginRO<>(socialUserTokenDO.getToken(), userRO);
    }
}