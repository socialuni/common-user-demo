package com.socialuni.common.user.demo;

public class UserSdkException extends Exception{
    public UserSdkException(String message) {
        super(message);
    }
}
