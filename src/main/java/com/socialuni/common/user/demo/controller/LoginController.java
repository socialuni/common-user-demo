package com.socialuni.common.user.demo.controller;

import com.socialuni.common.user.demo.ResultRO;
import com.socialuni.common.user.demo.service.LoginService;
import com.socialuni.common.user.sdk.model.RO.CommonUserRO;
import com.socialuni.uniapp.sdk.exception.UniSdkException;
import com.socialuni.uniapp.sdk.model.UniProviderLoginQO;
import com.socialuni.common.user.sdk.model.RO.UniLoginRO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("login")
public class LoginController {
    @Resource
    LoginService loginService;

    @PostMapping("providerLogin")
    public ResultRO<UniLoginRO<CommonUserRO>> providerLogin(@RequestBody UniProviderLoginQO loginQO) throws UniSdkException {

        UniLoginRO<CommonUserRO> loginRO = loginService.providerLogin(loginQO);

        return new ResultRO<>(loginRO);
    }
}