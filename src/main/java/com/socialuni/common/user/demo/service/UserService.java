package com.socialuni.common.user.demo.service;

import com.socialuni.common.user.demo.UserSdkException;
import com.socialuni.common.user.model.DO.CommonTokenDO;
import com.socialuni.common.user.model.DO.CommonUserDO;
import com.socialuni.common.user.sdk.manage.CommonUserDOGet;
import com.socialuni.common.user.sdk.model.RO.CommonUserRO;
import com.socialuni.common.user.sdk.repository.CommonTokenRepository;
import com.socialuni.common.user.sdk.utils.CommonTokenUtil;
import com.socialuni.common.user.sdk.utils.model.CommonUserROUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserService {
    @Resource
    CommonTokenRepository commonTokenRepository;
    @Resource
    CommonUserDOGet commonUserDOGet;

    public CommonUserRO getMineUser() throws UserSdkException {
        String token = CommonTokenUtil.getToken();
        CommonTokenDO tokenDO = commonTokenRepository.findOneByToken(token);
        if (tokenDO == null) {
            throw new UserSdkException("错误的信息");
        }

        CommonUserDO user = commonUserDOGet.get(tokenDO.getUserId());

        CommonUserRO userRO = CommonUserROUtil.getMineUser(user);
        return userRO;
    }

}