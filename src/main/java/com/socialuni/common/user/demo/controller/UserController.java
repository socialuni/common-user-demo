package com.socialuni.common.user.demo.controller;

import com.socialuni.common.user.demo.ResultRO;
import com.socialuni.common.user.demo.UserSdkException;
import com.socialuni.common.user.sdk.model.RO.CommonUserRO;
import com.socialuni.uniapp.sdk.exception.UniSdkException;
import com.socialuni.common.user.demo.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("user")
public class UserController {
    @Resource
    UserService userService;

    @PostMapping("getMineUser")
    public ResultRO<CommonUserRO> getMineUser() throws UserSdkException {
        CommonUserRO userRO = userService.getMineUser();
        return new ResultRO<>(userRO);
    }
}